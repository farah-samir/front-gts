import { Component, OnInit } from '@angular/core';
import { Paie } from '../interface/paie';
import { PaieService } from '../services/paie.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-showpaie',
  templateUrl: './showpaie.component.html',
  styleUrls: ['./showpaie.component.css']
})
export class ShowpaieComponent implements OnInit {

  paie:Paie = {
  home_id: null,
  date :null,
  mois:null,
  salaire:null,
  prime:null,
  affiche:null,
 

  };
  editing : boolean = false;
  paies:Paie[];
  id: any;


  constructor(private paieService:PaieService , private activatedRoute:ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.paieService.get().subscribe((data:Paie[])=>{
      this.paies=data;
      // BUG: had l partie ti3emrhom
    this.paie = this.paies.find((m)=>{return m.id == this.id});
    ;
    },(error)=>{
      console.log(error);
    });
  }
   }

  ngOnInit() {
  }

}
