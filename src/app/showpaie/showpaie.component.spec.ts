import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowpaieComponent } from './showpaie.component';

describe('ShowpaieComponent', () => {
  let component: ShowpaieComponent;
  let fixture: ComponentFixture<ShowpaieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowpaieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowpaieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
