import { Component, OnInit } from '@angular/core';
import { Paie } from '../interface/paie';
import { PaieService } from '../services/paie.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { Home } from '../interface/home';
import { HomeService } from '../services/home.service';

@Component({
  selector: 'app-paie',
  templateUrl: './paie.component.html',
  styleUrls: ['./paie.component.css']
})
export class PaieComponent implements OnInit {

  salaire:string;
  prime:string;
  affiche : number;

  homes: Home[];

  home:Home = {
    cin:null,
    nom:null,
    prenom:null,
    adresse:null,
    date:null,
    telephone:null,
    situations:null,
    civilite:null,
    email:null,
    code:null,
    departement:null,
    duration:null,
    lieu:null,
    mois:null,
    sl:null,
    dated:null,
    datef:null,
    semaine:null,
    typ:null,
    horaire:null,
    poste:null,
    paie:null,
    numero:null,
    prime:null,
};

  paie:Paie = {
    home_id: " ",
  salaire :" ",
  date:null,
  mois:" ",
  prime:" ",
  affiche:null,
 

  };
  editing : boolean = false;
  paies : Paie[];
  id: any;

  constructor(private paieService:PaieService , private homeService:HomeService, 
    private activatedRoute:ActivatedRoute , private router:Router
    ) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.editing = true;
    this.paieService.get().subscribe((data:Paie[])=>{
      this.paies=data;
    this.paie = this.paies.find((m)=>{return m.id == this.id});
    console.log(this.paie);
    },(error)=>{
      console.log(error);
    });
  }else {
    this.editing = false;
  }
  this.getService();

  }

  ngOnInit() {
    
  }


  refresh(): void {
    window.location.reload();
}

ref(){
  this.router.navigate(['/paie']);
}
  getService(){
    this.homeService.get().subscribe((data:Home[])=>{
     this.homes = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }


  savePaie(){
    if (this.editing){
      this.paie.affiche = parseInt(this.paie.salaire) + parseInt(this.paie.prime) ;
      console.log(this.paie.affiche);
      this.paieService.put(this.paie).subscribe((data)=>{
        alert('salaire a été modifié ');
        this.router.navigate(['/salaire']);
         
        console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });


    }else {
      this.paie.affiche = parseInt(this.paie.salaire) + parseInt(this.paie.prime) ;
      console.log(this.paie.affiche);

      this.paieService.save(this.paie).subscribe((data)=>{
        alert('nouveau salaire ');
        this.router.navigateByUrl("/paie", {skipLocationChange:true});
       
       console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });

    }

  }

}
