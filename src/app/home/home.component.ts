import { Component, OnInit } from '@angular/core';
import { Home } from '../interface/home';
import { HomeService } from '../services/home.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  home:Home = {
    cin:null,
    nom:null,
    prenom:null,
    adresse:null,
    date:null,
    telephone:null,
    situations:null,
    civilite:null,
    email:null,
    code:null,
    departement:null,
    duration:null,
    lieu:null,
    mois:null,
    sl:null,
    dated:null,
    datef:null,
    semaine:null,
    typ:null,
    horaire:null,
    poste:null,
    paie:null,
    numero:null,
    prime:null,

  };
  editing : boolean = false;
  homes:Home[];
  id: any;

  constructor(private homeService:HomeService , private activatedRoute:ActivatedRoute ,private router:Router) {
  this.id = this.activatedRoute.snapshot.params['id'];
if(this.id){
  this.editing = true;
  this.homeService.get().subscribe((data:Home[])=>{
    this.homes=data;
  this.home = this.homes.find((m)=>{return m.id == this.id});
  console.log(this.home);
  },(error)=>{
    console.log(error);
  });
}else {
  this.editing = false;
}

}

  ngOnInit() {

  }

saveAjoute(){
  if (this.editing){
    this.homeService.put(this.home).subscribe((data)=>{
      alert('enregistrement actualisé');
        window.location.href='/notes';
        this.router.navigate(['/recrute']);

      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });


  }else {
    this.homeService.save(this.home).subscribe((data)=>{
      alert('enregistrement save');
       this.router.navigate(['/recrute']);
                     console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });

  }

}
  }
