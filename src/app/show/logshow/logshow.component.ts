import { User } from './../../interface/user';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-logshow',
  templateUrl: './logshow.component.html',
  styleUrls: ['./logshow.component.css']
})
export class LogshowComponent implements OnInit {

  user:User = {
    name: null,
    email: null,
    droit:null,
    password:null,
    c_password:null,

  };
  editing : boolean = false;
  users:User[];
  id: any;

  constructor(private rnService:UserService , private activatedRoute:ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.rnService.get().subscribe((data:User[])=>{
      this.users=data;
      // BUG: had l partie ti3emrhom
    this.user = this.users.find((m)=>{return m.id == this.id});
    ;
    },(error)=>{
      console.log(error);
    });
  }

  }

  ngOnInit() {
  }

}
