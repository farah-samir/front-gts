import { User } from './../../interface/user';
import { Router } from '@angular/router';
import { Form } from './../../interface/form';
import { JarwisService } from './../../services/jarwis.service';

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-loginshow',
  templateUrl: './loginshow.component.html',
  styleUrls: ['./loginshow.component.css']
})
export class LoginshowComponent implements OnInit {
  editing : boolean = false;

  
  users: User[];
  user:User;

  constructor(private jarwisService:JarwisService ,private router:Router) {
  this.getService();
 

   }
 

   getService(){
     this.jarwisService.get().subscribe((data:User[])=>{
      this.users = data;
     
         }, (error)=>{
            console.log(error);
                  alert('error');
                     });
  } 
  

  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.jarwisService.destroy(id).subscribe((data:User[])=>{
       this.users = data;
      
       this.getService();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }

}

savePaie(){
  if (this.editing){
   
    this.jarwisService.put(this.user).subscribe((data)=>{
      alert('salaire a été modifié ');
      this.router.navigate(['/salaire']);
       
      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });


  }else {
   

    this.jarwisService.save(this.user).subscribe((data)=>{
      alert('nouveau salaire ');
      this.router.navigate(['/salaire']) ;
     console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });

  }

}




}