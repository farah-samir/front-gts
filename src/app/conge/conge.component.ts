import { Component, OnInit } from '@angular/core';
import { Conge } from '../interface/conge';
import { CngService } from '../services/cng.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { daysInYear } from 'ngx-bootstrap/chronos/units/year';
import { frLocale } from 'ngx-bootstrap';
import {formatDate} from '@angular/common';
 import { registerLocaleData } from '@angular/common';
 import * as moment from 'moment';
import 'moment/locale/fr';
@Component({
  selector: 'app-conge',
  templateUrl: './conge.component.html',
  styleUrls: ['./conge.component.css']
})
export class CongeComponent implements OnInit {
number1:any;
number2:Date;
resultats :Date ;
options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
dateFr:moment.Moment;
afficheDateFr:string;
conge:Conge = {
cin: null,
prenom :null,
nom:null,
genreconge:null,
congeformation:null,
congecitoyen:null,
dates:null,
number1:null,
datee:null,
number2:null,
resultats:null,
};
editing : boolean = false;
conges:Conge[];
id: any;

  constructor(private cngService:CngService , private activatedRoute:ActivatedRoute ,private router:Router) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.editing = true;
    this.cngService.get().subscribe((data:Conge[])=>{
      this.conges=data;
    this.conge = this.conges.find((m)=>{return m.id == this.id});
    console.log(this.conge);
    },(error)=>{
      console.log(error);
    });
  }else {
    this.editing = false;
  }


   }
  ngOnInit() {
  }


saveConge(){
  if (this.editing){

   

    this.cngService.put(this.conge).subscribe((data)=>{
      alert('enregistrement actualisé');
      this.router.navigate(['/affiche'])
      ;
      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });


  }else {
   
     this.conge.resultats = (new Date());
    
   
   // this.conge.resultats.setDate(this.conge.resultats.getDate()
    // + this.conge.number1);
      this.conge.resultats.setDate(this.conge.resultats.getDate() + this.conge.number1);
    // new Date(localStorage.getItem("requestDate")).getDate() + 1

    // console.log(this.conge.resultats.toLocaleDateString('fr-FR',this.options));
    // this.dateFr= this.conge.resultats.toLocaleDateString('fr-FR',this.options);
    this.dateFr = moment(this.conge.resultats);
    this.afficheDateFr=this.dateFr.lang('fr').format('LLLL');
    this.cngService.save(this.conge).subscribe((data)=>{
      alert('enregistrement save');
      this.router.navigate(['/affiche'])
      ;
      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });

  }

}




  }