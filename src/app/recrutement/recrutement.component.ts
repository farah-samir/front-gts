import { Component, OnInit } from '@angular/core';

import { Home } from '../interface/home';
import { HomeService } from '../services/home.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-recrutement',
  templateUrl: './recrutement.component.html',
  styleUrls: ['./recrutement.component.css']
})
export class RecrutementComponent implements OnInit {

  homes: Home[];
  cin:string;

  constructor(private homeService:HomeService) {
    this.getService();
  }
  getService(){
    this.homeService.get().subscribe((data:Home[])=>{
     this.homes = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  searchs(){
    if(this.cin != ""){
      this.homes= this.homes.filter(res=>{
        return res.cin.toLocaleLowerCase().match(this.cin.toLocaleLowerCase());
      }
    );
  }else if (this.cin == ""){
this.getService();
  }

  }

  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.homeService.destroy(id).subscribe((data:Home[])=>{
       this.homes = data;
       this.getService();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }

}
