import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Log } from '../interface/log';
import { LogService } from '../services/log.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  log:Log = {

  email: null,
  password :null,

  };

  constructor(private logS:LogService , private activatedRoute:ActivatedRoute ,private router:Router){}


  ngOnInit() {


  }

  savelog(){

        this.logS.save(this.log).subscribe((data)=>{
          alert('bienvenu MR Admin');
           this.router.navigate(['/welcome']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('Verifie votre mots de passe');
        });

}


}
