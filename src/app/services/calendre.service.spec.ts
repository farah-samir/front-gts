import { TestBed } from '@angular/core/testing';

import { CalendreService } from './calendre.service';

describe('CalendreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalendreService = TestBed.get(CalendreService);
    expect(service).toBeTruthy();
  });
});
