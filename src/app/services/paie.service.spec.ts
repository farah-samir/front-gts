import { TestBed } from '@angular/core/testing';

import { PaieService } from './paie.service';

describe('PaieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaieService = TestBed.get(PaieService);
    expect(service).toBeTruthy();
  });
});
