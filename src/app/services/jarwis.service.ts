import { User } from './../interface/user';
import { Form } from './../interface/form';
import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders } from '@angular/common/http';

@Injectable()
export class JarwisService {
  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  signup(data) {
    return this.http.post(`${this.baseUrl}/signup`, data)
  }

  login(data) {
    return this.http.post(`${this.baseUrl}/login`, data)
  }

  sendPasswordResetLink(data) {
    return this.http.post(`${this.baseUrl}/sendPasswordResetLink`, data)
  }

  changePassword(data) {
    return this.http.post(`${this.baseUrl}/resetPassword`, data)
  }
  get() {
    return this.http.get(this.baseUrl+'/getuser');
    }



    getOne(id:string) {
      return this.http.get(this.baseUrl+`/user/${id}`);
      }
  save(user:User){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post(this.baseUrl + '/user', user, {headers: headers});
  }
  put(user:User){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.put(this.baseUrl + '/user/' + user.id , user, {headers: headers});
  }
  destroy(id){
      return this.http.delete(this.baseUrl+'/user/'+ id);

  }

}
