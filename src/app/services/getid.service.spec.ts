import { TestBed } from '@angular/core/testing';

import { GetidService } from './getid.service';

describe('GetidService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetidService = TestBed.get(GetidService);
    expect(service).toBeTruthy();
  });
});
