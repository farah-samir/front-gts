import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Log } from '../interface/log';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
  get() {
  return this.httpClient.get(this.API_ENDPOINT+'/login');
  }

  save(log:Log){
  const headers = new HttpHeaders({'Content-Type':'application/json'});
  return this.httpClient.post(this.API_ENDPOINT + '/login', log, {headers: headers});

  }
}
