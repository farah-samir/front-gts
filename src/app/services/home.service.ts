import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Home } from '../interface/home';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/home');
    }
    getOne(id:string) {
      return this.httpClient.get(this.API_ENDPOINT+`/home/${id}`);
      }
  save(home:Home){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/home', home, {headers: headers});
  }
  put(home:Home){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/home/' + home.id , home, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/home/'+ id);

  }
}
