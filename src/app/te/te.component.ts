import { Component, OnInit } from '@angular/core';
import { Ordre } from '../interface/ordre';
import { OrdreService } from '../services/ordre.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-te',
  templateUrl: './te.component.html',
  styleUrls: ['./te.component.css']
})
export class TeComponent implements OnInit {

  ordre:Ordre = {
    home_id: null,
    horaire :null,
    date:null,
    sujet:null,
    transport:null,
    lieu:null,
    };
  editing : boolean = false;
  ordres:Ordre[];
  id: any;


  constructor(private ordreService:OrdreService , private activatedRoute:ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.ordreService.get().subscribe((data:Ordre[])=>{
      this.ordres=data;
      // BUG: had l partie ti3emrhom
    this.ordre = this.ordres.find((m)=>{return m.id == this.id});
    ;
    },(error)=>{
      console.log(error);
    });
  }
   }

  ngOnInit() {
  }

}
