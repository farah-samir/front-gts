import { Component, OnInit } from '@angular/core';
import { Ordre } from '../interface/ordre';
import { OrdreService } from '../services/ordre.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-mission',
  templateUrl: './mission.component.html',
  styleUrls: ['./mission.component.css']
})
export class MissionComponent implements OnInit {
  home_id:string;
  ordres:Ordre[];

  constructor(private ordreService:OrdreService) {
  this.getOrdre();
   }

  ngOnInit() {
  }

  search(){
    if(this.home_id != ""){
      this.ordres= this.ordres.filter(res=>{
        return res.home_id.toLocaleLowerCase().match(this.home_id.toLocaleLowerCase());
      }
    );
  }else if (this.home_id == ""){
 this.getOrdre();
  }

  }

  getOrdre(){
    this.ordreService.get().subscribe((data:Ordre[])=>{
     this.ordres = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.ordreService.destroy(id).subscribe((data:Ordre[])=>{
       this.ordres = data;
       this.getOrdre();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }





}
