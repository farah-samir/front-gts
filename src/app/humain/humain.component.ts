import { Component, OnInit } from '@angular/core';
import { Reunion } from '../interface/reunion';
import { ReunionService } from '../services/reunion.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-humain',
  templateUrl: './humain.component.html',
  styleUrls: ['./humain.component.css']
})
export class HumainComponent implements OnInit {
  reunion:Reunion = {
    home_id:null,
  horaire :null,
  date:null,
  sujet:null,

  };
  editing : boolean = false;
  reunions:Reunion[];
  id: any;

  constructor(private rnService:ReunionService , private activatedRoute:ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.rnService.get().subscribe((data:Reunion[])=>{
      this.reunions=data;
      // BUG: had l partie ti3emrhom
    this.reunion = this.reunions.find((m)=>{return m.id == this.id});
    ;
    },(error)=>{
      console.log(error);
    });
  }

  }

  ngOnInit() {
  }

}
