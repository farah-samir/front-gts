import { Component, OnInit } from '@angular/core';
import { Reunion } from '../interface/reunion';
import { ReunionService } from '../services/reunion.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { Home } from '../interface/home';
import { HomeService } from '../services/home.service';




@Component({
  selector: 'app-reunion',
  templateUrl: './reunion.component.html',
  styleUrls: ['./reunion.component.css']
})
export class ReunionComponent implements OnInit {
  homes: Home[];

  home:Home = {
    cin:null,
    nom:null,
    prenom:null,
    adresse:null,
    date:null,
    telephone:null,
    situations:null,
    civilite:null,
    email:null,
    code:null,
    departement:null,
    duration:null,
    lieu:null,
    mois:null,
    sl:null,
    dated:null,
    datef:null,
    semaine:null,
    typ:null,
    horaire:null,
    poste:null,
    paie:null,
    numero:null,
    prime:null,
};

  reunion:Reunion = {
    home_id:null,
  horaire :null,
  date:null,
  sujet:null,

  };
  editing : boolean = false;
  reunions:Reunion[];
  id: any;
  constructor( private rnService:ReunionService , private homeService:HomeService,
     private activatedRoute:ActivatedRoute , private router:Router) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.editing = true;
    this.rnService.get().subscribe((data:Reunion[])=>{
      this.reunions=data;
    this.reunion = this.reunions.find((m)=>{return m.id == this.id});
    console.log(this.reunion);
    },(error)=>{
      console.log(error);
    });
  }else {
    this.editing = false;
  }


  


}


  ngOnInit() {
    this.getService();
  }


saveReunion(){
  if (this.editing){
    this.rnService.put(this.reunion).subscribe((data)=>{
      alert('enregistrement actualisé');
      this.router.navigate(['/union']);
      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });


  }else {

    this.rnService.save(this.reunion).subscribe((data)=>{
      alert('enregistrement save');
      this.router.navigate(['/union']);
   
            console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });

  }

}




getService(){
  this.homeService.get().subscribe((data:Home[])=>{
   this.homes = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}

}
