export interface Reunion {
  id?: number;
  home_id:any;
  sujet:string;
  horaire: string;
  date: number;
  personne?:string;
}
