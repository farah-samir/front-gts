export interface User {
  id?: number;
  name: string;
  email: string;
  droit:string;
  password:string;
  c_password:string;
}
