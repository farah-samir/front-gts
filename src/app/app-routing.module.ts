import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { RequestRestComponent } from './components/password/request-rest/request-rest.component';
import { ResponseRestComponent } from './components/password/response-rest/response-rest.component';
import { AuthComponent } from './components/auth/auth.component';
import { AfterLoginService } from './services/after-login.service';
import { BeforeLoginService } from './services/before-login.service';
import { Observable } from 'rxjs';


const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
      canActivate: [BeforeLoginService]

  },
  
  {
    path: 'signup',
    component: SignupComponent,
  canActivate: [BeforeLoginService]


  },

  {
    path: 'request-password-reset',
    component: RequestRestComponent,
  canActivate: [BeforeLoginService]


  },
  {
    path: 'response-password-reset',
    component: ResponseRestComponent,
  canActivate: [BeforeLoginService]
  }
 


    




];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)

  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
