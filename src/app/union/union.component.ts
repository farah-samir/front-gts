import { Home } from './../interface/home';
import { HomeService } from './../services/home.service';
import { Component, OnInit } from '@angular/core';
import { Reunion } from '../interface/reunion';
import { ReunionService } from '../services/reunion.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-union',
  templateUrl: './union.component.html',
  styleUrls: ['./union.component.css']
})
export class UnionComponent implements OnInit {
  nom:string;
 
  reunions: Reunion[];
  homes: Home[];
  home:Home;
  reunion:Reunion = {
    home_id:"",
     sujet:"",
     horaire: "",
     date:0,
     personne:""
  }
  constructor(private rnService:ReunionService,private homeService:HomeService) {
  this.getService();
  this.getHome();

   }
   searchs(){
     if(this.nom != ""){
       this.reunions= this.reunions.filter(res=>{
         //return res.date.toLocaleString().match(this.nom.toLocaleLowerCase());

         return  res.date.toLocaleString() >= this.nom;
       }
     );
   }else if (this.nom== ""){
  this.getService();

   }
   }

   getService(){
     this.rnService.get().subscribe((data:Reunion[])=>{
      this.reunions = data;
     
         }, (error)=>{
            console.log(error);
                  alert('error');
                     });
   }
   getHome(){
    this.homeService.get().subscribe((data:Home[])=>{
     this.homes = data;
    
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }

  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.rnService.destroy(id).subscribe((data:Reunion[])=>{
       this.reunions = data;
      
       this.getService();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }










}
