import { Component, OnInit } from '@angular/core';
import { Home } from '../interface/home';
import { HomeService } from '../services/home.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profils',
  templateUrl: './profils.component.html',
  styleUrls: ['./profils.component.css']
})
export class ProfilsComponent implements OnInit {
  homes: Home[];
  id: any;
  editing : boolean = false;
  home:Home = {
    cin:null,
    nom:null,
    prenom:null,
    adresse:null,
    date:null,
    telephone:null,
    situations:null,
    civilite:null,
    email:null,
    code:null,
    departement:null,
    duration:null,
    lieu:null,
    mois:null,
    sl:null,
    dated:null,
    datef:null,
    semaine:null,
    typ:null,
    horaire:null,
    poste:null,
    paie:null,
    numero:null,
    prime:null,
}
  constructor(private homeService:HomeService , private activatedRoute:ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.homeService.get().subscribe((data:Home[])=>{
      this.homes=data;
      // BUG: had l partie ti3emrhom
    this.home = this.homes.find((m)=>{return m.id == this.id});
    ;
    },(error)=>{
      console.log(error);
    });
  }

}


ngOnInit() {  }

}
