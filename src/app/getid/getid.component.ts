import { Component, OnInit } from '@angular/core';
import { Reunion } from '../interface/reunion';
import { ReunionService } from '../services/reunion.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-getid',
  templateUrl: './getid.component.html',
  styleUrls: ['./getid.component.css']
})
export class GetidComponent implements OnInit {

  reunion:Reunion = {
  home_id:null,
  horaire :null,
  date:null,
  sujet:null,

  };
  value = '';
  editing : boolean = false;
  reunions:Reunion[];
  id: any;
get:any = {
  id:0,
  sujet:"",
  specialite:""
  
}
  constructor(private rnService:ReunionService , private activatedRoute:ActivatedRoute) {

  // if(this.id){
  //   this.rnService.get().subscribe((data:Reunion[])=>{
  //     this.reunions=data;
  //     // BUG: had l partie ti3emrhom
  //   this.reunion = this.reunions.find((m)=>{return m.id == this.id});
  //   ;
  //   },(error)=>{
  //     console.log(error);
  //   });
  // }

  }

  ngOnInit() {
  }
  
 getOneReunion(form){
   
   
if(form.valid){
  this.rnService.get().subscribe((data:Reunion[])=>{

    this.reunions=data;
    console.log(this.reunions)
    // BUG: had l partie ti3emrhom
   
  this.reunion = this.reunions.find((m)=>{return m.id == form.value.id});
  //  this.reunion = this.reunions.find((m)=>{return m.id == form.value.id});

   this.get.sujet = this.reunion.sujet
  ;
  },(error)=>{
    console.log(error);
  });
}
 
 } 

  onKeydown(event) {
    this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.rnService.get().subscribe((data:Reunion[])=>{
        this.reunions=data;
        // BUG: had l partie ti3emrhom
      this.reunion = this.reunions.find((m)=>{return m.id == this.id});
      ;
      },(error)=>{
        console.log(error);
      });
    }
  }

}

