
import {Component, OnInit,ViewChild} from '@angular/core';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { EventInput } from '@fullcalendar/core';
import { Calendre } from '../interface/calendre';
import { CalendreService } from '../services/calendre.service';
import { Observable } from 'rxjs';
import { ActivatedRoute,Router } from '@angular/router';
import {InputTextModule} from 'primeng/inputtext';



@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  @ViewChild('calendar') calendarComponent: DemoComponent; 

  disabled: boolean = false;

   

  display: boolean = false;

  events: any[];
  clickMessage = '';

    options: any;

    calendarEvents: any[] = [
     
    ];


    calendre:Calendre = {
      start: null,
      title:null
  
    };
    editing : boolean = false;
    calendres:Calendre[];
    id: any;


    calendarPlugins = [dayGridPlugin, timeGridPlugin, interactionPlugin];

    constructor(private calendreService:CalendreService , private activatedRoute:ActivatedRoute ,
      private router:Router
      ) {
        
        this.getConge();
        this.getDa();

      this.id = this.activatedRoute.snapshot.params['id'];
    if(this.id){
      this.editing = true;
      this.calendreService.get().subscribe((data:Calendre[])=>{
        this.calendres=data;
      this.calendre = this.calendres.find((m)=>{return m.id == this.id});
      console.log(this.calendre);
      },(error)=>{
        console.log(error);
      });
    }else {
      this.editing = false;
    }
  
 
    }

  
  ngOnInit() {
this.getDa();
this.getConge();
    this.options = {
      
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
     defaultDate :Date.now(),
  
      header: {
          left: 'prev,next',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
      },
      dateClick: (e) =>  {
        //handle date click
    }
  };
}

showDialog() {
  this.display = true;
}


getDa(){
  this.calendreService.get().subscribe((data:Calendre[])=>{
   this.calendarEvents = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}

getConge(){
  this.calendreService.get().subscribe((data:Calendre[])=>{
   this.calendres = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}






toggleDisabled() {
  this.disabled = !this.disabled;
}




saveFull(){
  if (this.editing){
    this.calendreService.put(this.calendre).subscribe((data)=>{
      this.getConge();
      this.getDa();
      this.router.navigate(['/demo']);      
      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });


  }else {
  
    this.calendreService.save(this.calendre).subscribe((data)=>{
      alert('enregistrement actualisé ');
      this.getConge();
    this.getDa();
      this.router.navigate(['/demo']);     
      console.log(data);
    },(error)=>{
      console.log(error);
      alert('eroor');
    });

  }

}



destroy(id) {
  this.calendreService.destroy(id).subscribe((data:Calendre[])=>{
    this.calendres = data;
    this.getConge();
    this.getDa();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
}



  

}
