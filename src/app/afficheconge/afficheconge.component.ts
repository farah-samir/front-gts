import { ExcelService } from './../services/excel.service';
import { Component, OnInit } from '@angular/core';
import { Conge } from '../interface/conge';
import { CngService } from '../services/cng.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-afficheconge',
  templateUrl: './afficheconge.component.html',
  styleUrls: ['./afficheconge.component.css']
})
export class AffichecongeComponent implements OnInit {
  cin:string;
  date1:string;
  date2:string;

  conges: Conge[];
  excel=[];

  constructor(private cngService:CngService ,private excelService:ExcelService) {
  this.getConge();
  this. getData();




   
}



   
   searchs(){
    if(this.cin != "" && this.date1 != "" && this.date2 != ""){
      this.conges= this.conges.filter(res=>{
        //return res.date.toLocaleString().match(this.nom.toLocaleLowerCase());

        return  res.cin.toLocaleString() == this.cin
        && (res.datee.toLocaleString() >= this.date1
        && res.datee.toLocaleString() <= this.date2);
      }



    );
  }else if (this.cin = "" ){
 this.getConge();

  }
  }


  getData(){

    this.cngService.get().subscribe((data:Conge[]) => {
      this.conges = data;
      data.forEach(row => {
        this.excel.push(row);
      });
     });  
  
  
  }

  getConge(){
    this.cngService.get().subscribe((data:Conge[])=>{
     this.conges = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  ngOnInit() {
  
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.cngService.destroy(id).subscribe((data:Conge[])=>{
       this.conges = data;
       this.getConge();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }

  exportAsXLSX():void {
    this.excelService.exportAsExcelFile(this.conges, 'sample');
  }

  exportAsXLSXe(id):void {
    this.excelService.exportAsExcelFile(this.conges, 'sample');
  }


}
