import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichecongeComponent } from './afficheconge.component';

describe('AffichecongeComponent', () => {
  let component: AffichecongeComponent;
  let fixture: ComponentFixture<AffichecongeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichecongeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichecongeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
