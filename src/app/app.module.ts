
import { BrowserModule } from '@angular/platform-browser';
import {NgModule , LOCALE_ID} from '@angular/core';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';

import { FlatpickrModule } from 'angularx-flatpickr';
import {  CalendarModule,DateAdapter } from 'angular-calendar';


import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { RecrutementComponent } from './recrutement/recrutement.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatStepperModule} from '@angular/material/stepper';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { OrderModule } from 'ngx-order-pipe';

import { NgxPaginationModule } from 'ngx-pagination';

import { ReactiveFormsModule } from '@angular/forms';
import { CongeComponent } from './conge/conge.component';
import { PersonneComponent } from './personne/personne.component';
import { AffichecongeComponent } from './afficheconge/afficheconge.component';
import { TestComponent } from './test/test.component';
import { ProfilsComponent } from './profils/profils.component';
import {DatePipe} from '@angular/common';
import { ReunionComponent } from './reunion/reunion.component';
import { UnionComponent } from './union/union.component';
import { HumainComponent } from './humain/humain.component';
import { PaieComponent } from './paie/paie.component';
import { SalaireComponent } from './salaire/salaire.component';
import { ShowpaieComponent } from './showpaie/showpaie.component';

import { CalendarComponent } from './calendar/calendar.component';
import { EditorComponent } from './editor/editor.component';

import { DemoComponent } from './demo/demo.component';
import { OrdreComponent } from './ordre/ordre.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { EditorModule } from '@tinymce/tinymce-angular';
import { TeComponent } from './te/te.component';
import { NgxEditorModule } from 'ngx-editor';
import { NotesComponent } from './notes/notes.component';
import { MissionComponent } from './mission/mission.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { UserComponent } from './user/user.component';
import { RequestRestComponent } from './components/password/request-rest/request-rest.component';
import { ResponseRestComponent } from './components/password/response-rest/response-rest.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthComponent } from './components/auth/auth.component';

import { JarwisService } from './services/jarwis.service';
import { TokenService } from './services/token.service';

import { AuthService } from './services/auth.service';
import { AfterLoginService } from './services/after-login.service';
import { BeforeLoginService } from './services/before-login.service';
import { AuthGuard } from './auth.guard';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CalComponent } from './cal/cal.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { AccuielComponent } from './accuiel/accuiel.component';
import { GetidComponent } from './getid/getid.component';
import {CheckboxModule} from 'primeng/checkbox';
import { LoginshowComponent } from './show/loginshow/loginshow.component';
import { ReceptionComponent } from './news/reception/reception.component';
import { LogshowComponent } from './show/logshow/logshow.component';



registerLocaleData(localeFr, 'fr-FR');



const appRoutes: Routes = [


  { path : '', redirectTo: '/login', pathMatch: 'full', canActivate: [AuthGuard]},
  { path : '', redirectTo: '/login', pathMatch: 'full', canActivate: [AfterLoginService]},



  {
    path:'showlogin',

  component: LoginshowComponent,
    canActivate: [AfterLoginService]
},


{
  path:'logshow',

component: LogshowComponent,
  canActivate: [AfterLoginService]
},



{
  path:'reception',

component: ReceptionComponent,
  canActivate: [AfterLoginService]
},


  {
    path:'home',

  component: HomeComponent,
    canActivate: [AfterLoginService]
},

{ path:'getid',
canActivate: [AfterLoginService],
component:  GetidComponent  },

  { path:'cal',
    canActivate: [AfterLoginService],
   component:  CalComponent  },
   { path:'welcome',
    canActivate: [AfterLoginService],
   component:  AccuielComponent  },

   { path:'home/:id',
   canActivate: [AfterLoginService],
  component:  HomeComponent  },

{path:'conge',
  canActivate: [AfterLoginService],
component: CongeComponent},
{path:'conge/:id',
  canActivate: [AfterLoginService],
component: CongeComponent},

{path:'personne/:id',
  canActivate: [AfterLoginService],
component: PersonneComponent},
{path:'personne',
  canActivate: [AfterLoginService],
component: PersonneComponent},
// BUG: l'affichage de table pas de id mais pour modifie , il faut obligatoire mettre l'id devant (router )
// BUG: comme home/:id
{ path:'recrute',
  canActivate: [AfterLoginService],
component:  RecrutementComponent  },
{path:'affiche',component: AffichecongeComponent},

{path:'test',
  canActivate: [AfterLoginService],
component: TestComponent},
{path:'profils',
  canActivate: [AfterLoginService],
component: ProfilsComponent},
{path:'profils/:id',
  canActivate: [AfterLoginService],
component: ProfilsComponent},

{path:'reunion',
  canActivate: [AfterLoginService],
component: ReunionComponent},
  { path:'reunion/:id',
  canActivate: [AfterLoginService],
   component:  ReunionComponent  },
  { path:'union',
  canActivate: [AfterLoginService],
   component:  UnionComponent  },

  {path:'humain/:id',
  canActivate: [AfterLoginService],
  component: HumainComponent},
  {path:'humain',
  canActivate: [AfterLoginService],
  component: HumainComponent},
  {path:'paie',
  canActivate: [AfterLoginService],
  component:PaieComponent},
  {path:'paie/:id',
  canActivate: [AfterLoginService],
  component:PaieComponent},
  {path:'salaire',
  canActivate: [AfterLoginService],
  component:SalaireComponent},
    {path:'showpaie',
  canActivate: [AfterLoginService],
    component:ShowpaieComponent},
      {path:'showpaie/:id',
  canActivate: [AfterLoginService],
      component:ShowpaieComponent},
        {path:'cal',
  canActivate: [AfterLoginService],
        component:CalendarComponent},
        {path:'demo',
  canActivate: [AfterLoginService],
        component:DemoComponent},
        {path:'ordre',
  canActivate: [AfterLoginService],
        component:OrdreComponent},

        {path:'ordre/:id',
        canActivate: [AfterLoginService],
        component:OrdreComponent},

        {path:'editor',
  canActivate: [AfterLoginService],
        component:EditorComponent},
        {path:'editor/:id',
  canActivate: [AfterLoginService],
        component:EditorComponent},
        {path:'te/:id',
  canActivate: [AfterLoginService],
        component:TeComponent},
       {path:'notes',
  canActivate: [AfterLoginService],
        component: NotesComponent},
       {path:'mission',
  canActivate: [AfterLoginService],
        component: MissionComponent},


       {
         path: 'login',
         component: LoginComponent

       },
       {
         path: 'signup',
         component: SignupComponent

       },

       { path:'**', component: PagenotfoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
     RecrutementComponent,
    HomeComponent,
    CongeComponent,
    PersonneComponent,
    AffichecongeComponent,
    TestComponent,
    ProfilsComponent,
    ReunionComponent,
    UnionComponent,
    HumainComponent,
    PaieComponent,
    SalaireComponent,
    ShowpaieComponent,
   
    CalendarComponent,
    DemoComponent,
    OrdreComponent,
     EditorComponent,
     TeComponent,
     NotesComponent,
     MissionComponent,

     LoginFormComponent,
     UserComponent,
     RequestRestComponent,
     ResponseRestComponent,
     NavbarComponent,
     LoginComponent,
     SignupComponent,
     AuthComponent,
     PagenotfoundComponent,
     CalComponent,
     AccuielComponent,
     GetidComponent,
     LoginshowComponent,
     ReceptionComponent,
     LogshowComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
        BrowserAnimationsModule,
        MatStepperModule,
        NoopAnimationsModule,
        MatCheckboxModule,
        NgxPaginationModule,
        OrderModule,
        NgbPaginationModule, NgbAlertModule,
        ReactiveFormsModule,
        CommonModule,
        NgbModalModule,
        FlatpickrModule.forRoot(),
        CalendarModule.forRoot({
       provide: DateAdapter,
       useFactory: adapterFactory
     }),
     AngularEditorModule,
     EditorModule,
     NgxEditorModule,
     AppRoutingModule,
       SnotifyModule,
    
       FullCalendarModule,
       CheckboxModule
  


  ],
  providers: [DatePipe , JarwisService,
  TokenService,AuthService,
   AfterLoginService,BeforeLoginService,AuthGuard,
  { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
  SnotifyService,
  {
    provide: LOCALE_ID,
    useValue: 'fr-FR'
   }

],
  exports: [MatButtonModule, MatCheckboxModule ],
  bootstrap: [AppComponent]
})
export class AppModule { }
