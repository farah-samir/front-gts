import { Component, OnInit } from '@angular/core';
import { Ordre } from '../interface/ordre';
import { OrdreService } from '../services/ordre.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { Home } from '../interface/home';
import { HomeService } from '../services/home.service';


@Component({
  selector: 'app-ordre',
  templateUrl: './ordre.component.html',
  styleUrls: ['./ordre.component.css']
})
export class OrdreComponent implements OnInit {

  homes: Home[];

  home:Home = {
    cin:null,
    nom:null,
    prenom:null,
    adresse:null,
    date:null,
    telephone:null,
    situations:null,
    civilite:null,
    email:null,
    code:null,
    departement:null,
    duration:null,
    lieu:null,
    mois:null,
    sl:null,
    dated:null,
    datef:null,
    semaine:null,
    typ:null,
    horaire:null,
    poste:null,
    paie:null,
    numero:null,
    prime:null,
  };
  ordre:Ordre = {
  home_id: null,
  horaire :null,
  date:null,
  sujet:null,
  transport:null,
  lieu:null,
  };
  editing : boolean = false;
  ordres:Ordre[];
  id: any;
  constructor( private ordreService:OrdreService , private homeService:HomeService, 
    private activatedRoute:ActivatedRoute, private router:Router ) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.editing = true;
    this.ordreService.get().subscribe((data:Ordre[])=>{
      this.ordres=data;
    this.ordre = this.ordres.find((m)=>{return m.id == this.id});
    console.log(this.ordre);
    },(error)=>{
      console.log(error);
    });
  }else {
    this.editing = false;
  }
  this.getService();

   }

  ngOnInit() {
  }


  saveOrdre(){
    if (this.editing){
      this.ordreService.put(this.ordre).subscribe((data)=>{
        alert('enregistrement actualisé');
        this.router.navigate(['/mission']);
        console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });


    }else {

      this.ordreService.save(this.ordre).subscribe((data)=>{
        alert('enregistrement save');
        this.router.navigate(['/mission']);
        console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });

    }

  }

  getService(){
    this.homeService.get().subscribe((data:Home[])=>{
     this.homes = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }

}
