import { Component, OnInit } from '@angular/core';
import { Conge } from '../interface/conge';
import { CngService } from '../services/cng.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {
  conges: Conge[];
  id: any;
  editing : boolean = false;

  conge:Conge = {
  cin: null,
  prenom :null,
  nom:null,
  genreconge:null,
  congeformation:null,
  congecitoyen:null,
  dates:null,
  number1:null,
  datee:null,
  number2:null,
  resultats:null,
  };


  constructor(private cngService:CngService , private activatedRoute:ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.cngService.get().subscribe((data:Conge[])=>{
      this.conges=data;
      // BUG: had l partie ti3emrhom
    this.conge = this.conges.find((m)=>{return m.id == this.id});
    ;
    },(error)=>{
      console.log(error);
    });
  }


   }

  ngOnInit() {
  }



}
